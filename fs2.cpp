#pragma once
#ifndef UTILS_FS_H
#define UTILS_FS_H
#include <Arduino.h>
#include <SPIFFS.h>
#include "global/global.h"
#include "module/debug/debug.h"

class FileSystem {
	public:
		void init(Global *global);
		bool begin();
		String read(const String path);

	private:
		bool isInit = false;
		bool isBegin = false;
		Global *global;
};

void FileSystem::init(Global *global) {
	if (!isInit) {
		this->global = global;
		isInit = true;
	}
}

bool FileSystem::begin() {
	if (!isInit) {
		return false;
	}

	if (!isBegin) {
		const bool result = SPIFFS.begin(true);

		if (!result) {
			global->mutex.serial->take();
			D_PRINTLN(F("Failed begin SPIFFS"));
			global->mutex.serial->give();
		}

		isBegin = true;

		return result;
	}

	if (isBegin) {
		return true;
	}

	return false;
}

String FileSystem::read(const String path) {
	String result;
	File file = SPIFFS.open(path.c_str(), FILE_READ);

	if (!file) {
		global->mutex.serial->take();
		D_PRINTFLN(PSTR("Failed open file %s"), path.c_str());
		global->mutex.serial->give();

		// file.close();

		return result;
	}

	while (file.available()) {
		result += char(file.read());
	}

	file.close();

	return result;
}

#endif