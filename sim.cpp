#include <Arduino.h>
#include <WiFi.h>
#include <WebServer.h>

const char *ssid = "ESP32";
const char *password = "12345678";

WebServer server(80);

void handle_OnConnect() {
	server.send(200, "text/html", "Hello server!");
}

void handle_NotFound(){
	server.send(404, "text/plain", "Not found");
}

void setup() {
	Serial.begin(115200);

	WiFi.softAP(ssid, password);
	delay(100);

	server.on("/", handle_OnConnect);
	server.onNotFound(handle_NotFound);

	server.begin();
	Serial.println("HTTP server started");
}

void loop() {
	server.handleClient();
}