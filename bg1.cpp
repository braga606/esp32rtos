#pragma once
#ifndef MODULE_WIFI_AP_H
#define MODULE_WIFI_AP_H
#include <Arduino.h>
#include <DNSServer.h>
// #include <ESPmDNS.h>
#include <WiFi.h>
#include <WebServer.h>
// #include <AsyncTCP.h>
// #include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#include "global/global.h"
#include "utils/filesystem/filesystem.h"

// class CaptiveRequestHandler: public AsyncWebHandler {
// 	public:
// 		CaptiveRequestHandler() {}

//   		virtual ~CaptiveRequestHandler() {}

// 		bool canHandle(AsyncWebServerRequest *request) {
// 			return true;
// 		}

// 		void handleRequest(AsyncWebServerRequest *request) {
// 			request->send(SPIFFS, "/index.html", "text/html");
// 		}
// };

class WifiAP {
	public:
		void init(Global *global, FileSystem *filesystem);
		void connect();
		void keepAlive();

	private:
		bool isInit = false;
		Global *global = nullptr;
		FileSystem *filesystem = nullptr;
		WebServer *server = nullptr;
		// AsyncWebServer *server = nullptr;
		DNSServer *dns = nullptr;
		// CaptiveRequestHandler *handler = nullptr;
};

void WifiAP::init(Global *global, FileSystem *filesystem) {
	if (!isInit) {
		this->global = global;
		this->filesystem = filesystem;
		this->server = new WebServer(this->global->config.wifi.ap.server_port);
		// this->server = new AsyncWebServer(this->global->config.wifi.ap.server_port);
		this->dns = new DNSServer();
		// this->handler = new CaptiveRequestHandler();

		if (this->global == nullptr) {
			D_PRINTLN(F("nullptr: global"));
		}

		if (this->server == nullptr) {
			D_PRINTLN(F("nullptr: server"));
		}

		if (this->dns == nullptr) {
			D_PRINTLN(F("nullptr: dns"));
		}

		// if (this->handler == nullptr) {
		// 	D_PRINTLN(F("nullptr: handler"));
		// }

		D_PRINTLN(F("Success init Wifi AP class"));

		isInit = true;
	}
}

void WifiAP::connect() {
	if (!isInit) {
		return;
	}

	D_PRINTLN(F("Start AP connect"));

	WiFi.disconnect(true, true);

	WiFi.mode(WIFI_MODE_AP);

	WiFi.enableAP(true);

	delay(1000);

	WiFi.softAPConfig(
		global->config.wifi.ap.ip,
		global->config.wifi.ap.ip,
		global->config.wifi.ap.subnet
	);

	D_PRINTLN(F("Set AP config"));

	WiFi.softAP(
		global->config.wifi.ap.ssid.c_str(),
		global->config.wifi.ap.password.c_str()
	);

	delay(1000);

	D_PRINTFLN(PSTR("Wifi AP IP: %s"), WiFi.softAPIP().toString().c_str());

	// MDNS.begin("esp32");

	dns->start(
		global->config.wifi.ap.dns_port,
		"*",
		global->config.wifi.ap.ip
	);

	// server->on("/ap", HTTP_POST, [](AsyncWebServerRequest *request) {}, NULL, [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
	// 	const char *payload = (const char *)data;
	// 	D_PRINTLN(payload);
	// 	request->send(200, "application/json", payload);
	// });

	// server->on("/ap", HTTP_POST, [](AsyncWebServerRequest *request) {
	// 	// global->mutex.serial->take();
	// 	D_PRINTLN(F("Recive POST request: /ap"));
	// 	// global->mutex.serial->give();

	// 	String plain;

	// 	if (request->hasParam("body", true, false)) {
	// 		plain = request->getParam("body", true, false)->value();
	// 		// global->mutex.serial->take();
	// 		D_PRINTLN(plain);
	// 		// global->mutex.serial->give();
	// 	}

	// 	request->send(200, "texl/plain", plain);
	// });

	// server->addHandler(handler).setFilter(ON_AP_FILTER);

	// server->onNotFound([](AsyncWebServerRequest *request) {
	// 	request->send(SPIFFS, "/index.html", "text/html");
	// });

	const String page = filesystem->read("/index.html");

	D_PRINTLN(page);

	server->on("/ap", HTTP_POST, [this]() {
		const String data = server->arg("plain");
		D_PRINTLN(data);
		server->send(200, "application/json", data);
	});

	server->onNotFound([this, page]() {
		server->send(200, "text/html", page);
	});

	server->begin();

	// MDNS.addService("http", "tcp", this->global->config.wifi.ap.server_port);
}

void WifiAP::keepAlive() {
	if (isInit) {
		dns->processNextRequest();
		server->handleClient();
	}
}

#endif