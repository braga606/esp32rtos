#pragma once
#ifndef UTILS_RTOS_SEMAPHORE_H
#define UTILS_RTOS_SEMAPHORE_H
#include <Arduino.h>
#include <FreeRTOS.h>

class SemaphoreBinary {
	public:
		SemaphoreBinary() {
			xSemaphore = xSemaphoreCreateBinary();
		}

		~SemaphoreBinary() {
			give();
		}

		BaseType_t give() {
			return xSemaphoreGive(xSemaphore);
		}

		BaseType_t take(const TickType_t xTicksToWait = portMAX_DELAY) {
			return xSemaphoreTake(xSemaphore, xTicksToWait);
		}

	private:
		SemaphoreHandle_t xSemaphore;
};

#endif