#pragma once
#ifndef UTILS_RTOS_QUEUE_H
#define UTILS_RTOS_QUEUE_H
#include <Arduino.h>
#include <FreeRTOS.h>

template<class T, size_t size, size_t length>
class QueueDynamic {
	public:
		QueueDynamic() {
			xHandle = xQueueCreate(length, size);
		}

		bool receive(T *value, TickType_t xTicksToWait = portMAX_DELAY) {
			return xQueueReceive(xHandle, value, xTicksToWait);
		}

		bool send(const T &value, TickType_t xTicksToWait = portMAX_DELAY) {
			return xQueueSend(xHandle, &value, xTicksToWait);
		}

	private:
		QueueHandle_t xHandle;
};

// template<class T, size_t size>
// class QueueStatic {
// 	public:
// 		void Queue() {
// 			xHandle = xQueueCreateStatic(
// 				size,
// 				sizeof(T),
// 				reinterpret_cast<uint8_t *>(xStorage),
// 				&xQueueDefinition
// 			);
// 		}

// 		~Queue() {
// 			xQueueRemoveFromSet(xQueueDefinition, xHandle);
// 		}

// 		bool receive(T *value, TickType_t xTicksToWait = portMAX_DELAY) {
// 			return xQueueReceive(xHandle, value, xTicksToWait);
// 		}

// 		bool send(const T &value, TickType_t xTicksToWait = portMAX_DELAY) {
// 			return xQueueSend(xHandle, &value, xTicksToWait);
// 		}

// 	private:
// 		QueueHandle_t xHandle;
// 		StaticQueue_t xQueueDefinition;
// 		T xStorage[size];
// };

#endif