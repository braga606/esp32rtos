#pragma once
#ifndef UTILS_RTOS_MUTEX_H
#define UTILS_RTOS_MUTEX_H
#include <Arduino.h>
#include <FreeRTOS.h>
#include "module/debug/debug.h"

class Mutex {
	public:
		Mutex() {
			xMutex = xSemaphoreCreateMutex();
		}

		~Mutex() {
			give();
		}

		BaseType_t take(TickType_t xTicksToWait = portMAX_DELAY) {
			return xSemaphoreTake(xMutex, xTicksToWait);
		}

		BaseType_t give() {
			return xSemaphoreGive(xMutex);
		}

	private:
		SemaphoreHandle_t xMutex;
};

class FakeMutex: public Mutex {
	public:
		BaseType_t take(TickType_t xTicksToWait = portMAX_DELAY) {
			return (BaseType_t)0;
		}

		BaseType_t give() {
			return (BaseType_t)0;
		}
};

#endif