#pragma once
#ifndef UTILS_RTOS_TASK_H
#define UTILS_RTOS_TASK_H
#include <Arduino.h>
#include <FreeRTOS.h>

class Task {
	public:
		static BaseType_t create(
			TaskFunction_t pvTaskCode,
			const char *pcName,
			TaskHandle_t *constpvCreatedTask = NULL,
			uint32_t usStackDepth = configMINIMAL_STACK_SIZE,
			void *pvParameters = NULL,
			UBaseType_t uxPriority = 1,
			BaseType_t xCoreID = 0
		) {
			return xTaskCreatePinnedToCore(
				pvTaskCode,
				pcName,
				usStackDepth,
				pvParameters,
				uxPriority,
				constpvCreatedTask,
				xCoreID
			);
		}

		static void wait(BaseType_t xClearCountOnExit = pdTRUE, TickType_t xTicksToWaitaskHandle_t = portMAX_DELAY) {
			ulTaskNotifyTake(xClearCountOnExit, xTicksToWaitaskHandle_t);
		}

		static void notify(TaskHandle_t xTaskToNofify) {
			xTaskNotifyGive(xTaskToNofify);
		}

		static void suspend(TaskHandle_t xTaskToSuspend = NULL) {
			vTaskSuspend(xTaskToSuspend);
		}

		static void resume(TaskHandle_t xTaskToResume = NULL) {
			vTaskResume(xTaskToResume);
		}

		static void delay(const uint32_t xTimeInMs = portMAX_DELAY) {
			const TickType_t xDelay = pdMS_TO_TICKS(xTimeInMs);
			vTaskDelay(xDelay);
		}

		static void yield() {
			taskYIELD();
		}

		static void remove(TaskHandle_t xTaskToDelete = NULL) {
			vTaskDelete(xTaskToDelete);
		}
};

#endif