#pragma once
#ifndef UTILS_RTOS_EVENTS_H
#define UTILS_RTOS_EVENTS_H
#include <Arduino.h>
#include <FreeRTOS.h>

class Events {
	public:
		Events() {
			xEventGroup = Events::create();
		}

		static EventGroupHandle_t create() {
			return xEventGroupCreate();
		}

		void wait(const EventBits_t uxBitsToWaitFor, TickType_t xTicksToWait = portMAX_DELAY, const BaseType_t xClearOnExit = true, const BaseType_t xWaitForAllBits = true) {
			xEventGroupWaitBits(xEventGroup, uxBitsToWaitFor, xClearOnExit, xWaitForAllBits, xTicksToWait);
		}

		void set(const EventBits_t uxBitsToSet) {
			xEventGroupSetBits(xEventGroup, uxBitsToSet);
		}

		void clear(const EventBits_t uxBitsToClear) {
			xEventGroupClearBits(xEventGroup, uxBitsToClear);
		}

	private:
		EventGroupHandle_t xEventGroup;
};

#endif