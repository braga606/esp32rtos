#pragma once
#ifndef UTILS_RTOS_TIMER_H
#define UTILS_RTOS_TIMER_H
#include <Arduino.h>
#include <FreeRTOS.h>

uint32_t xTimerIds = 0;

class Timer {
	public:
		bool isInit = false;
		bool isActive = false;
		uint32_t count = 0;

		void init(TimerCallbackFunction_t pxCallbackFunction, const char *pcTimerName, const uint32_t xTimeInMs, void *pvTimerID = (void *)++xTimerIds, const UBaseType_t uxAutoReload = pdTRUE);
		void start(bool clearCount = true);
		void stop(bool clearCount = true);
		void destory();
		void clearCount();
		uint32_t incrementCount();

	private:
		TimerHandle_t xHandle;
		TickType_t xPeriodInTicks;
};

void Timer::init(TimerCallbackFunction_t pxCallbackFunction, const char *pcTimerName, const uint32_t xTimeInMs, void *pvTimerID, const UBaseType_t uxAutoReload) {
	if (!isInit) {
		const TickType_t xTimerPeriodInTicks = pdMS_TO_TICKS(xTimeInMs);
		xPeriodInTicks = xTimerPeriodInTicks;
		isInit = true;
		xHandle = xTimerCreate(pcTimerName, xTimerPeriodInTicks, uxAutoReload, pvTimerID, pxCallbackFunction);
		isInit = true;
	}
}

void Timer::start(bool clearCount) {
	if (isInit && !isActive) {
		xTimerStart(xHandle, xPeriodInTicks);
		isActive = true;

		if (clearCount) {
			count = 0;
		}
	}
}

void Timer::stop(bool clearCount) {
	if (isInit && isActive) {
		xTimerStop(xHandle, xPeriodInTicks);
		isActive = false;

		if (clearCount) {
			count = 0;
		}
	}
}

uint32_t Timer::incrementCount() {
	count = count + 1;
	return count;
}

void Timer::clearCount() {
	if (isInit) {
		count = 0;
	}
}

void Timer::destory() {
	if (isInit) {
		xTimerDelete(xHandle, xPeriodInTicks);
		isInit = false;
		isActive = false;
		count = 0;
	}
}

#endif