#pragma once
#ifndef MODULE_MQTT_H
#define MODULE_MQTT_H
#include <map>
#include <Arduino.h>
#include <AsyncMqttClient.h>
#include "global/global.h"
#include "utils/json/json.h"
#include "module/debug/debug.h"

std::map<uint8_t, String> mqtt_topic_list_send {
	{ 0, "set/sensors/dht" }
};

std::map<uint8_t, String> mqtt_topic_list_recive {
	{ 0, "get/sensors/dht" }
};

enum class MqttTopicList_e {
	SensorDht = 0
};

class Mqtt {
	public:
		void init(Global *global);
		void connect();
		void disconnect();
		void request(MqttTopicList_e topic, const String data, uint8_t qos = 2, bool retain = false);
		void initEvents();
		void onMessage(const String topic, const String payload);

	private:
		Global *global;
		AsyncMqttClient *client;
		bool isInitEvents = false;
		bool isInit = false;
};

void Mqtt::init(Global *global) {
	if (!isInit) {
		this->global = global;
		client = new AsyncMqttClient();
		initEvents();
		isInit = true;
	}
}

void Mqtt::initEvents() {
	if (!isInitEvents) {
		client->onConnect([this](bool sessionPresent) {
			client->subscribe(mqtt_topic_list_recive[(uint8_t)MqttTopicList_e::SensorDht].c_str(), 2);
			global->events.group->set(global->events.mqtt.connected);
		});

		client->onDisconnect([this](AsyncMqttClientDisconnectReason reason) {
			global->events.group->set(global->events.mqtt.disconnected);
		});

		client->onMessage([this](char* category, char* data, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
			const String topic = String((const char *)category);
			const String payload = String((const char *)data);
			onMessage(topic, payload);
		});

		isInitEvents = true;
	}
}

void Mqtt::onMessage(const String topic, const String payload) {
	const JsonParseResult_t json = Json::parse(payload, 1024);

	global->mutex.serial->take();

	if (json.success) {
		D_PRINTFLN(PSTR("Mqtt recive message. Topic: %s, Payload: %s"), topic.c_str(), payload.c_str());
	} else {
		D_PRINTFLN(PSTR("Mqtt recive message. Failed parse json. Topic: %s, Payload: %s"), topic.c_str(), payload.c_str());
	}

	global->mutex.serial->give();
}

void Mqtt::connect() {
	disconnect();
	client->setClientId(global->config.id.c_str());
	client->setCredentials(global->config.mqtt.user.c_str(), global->config.mqtt.password.c_str());
	client->setServer(global->config.mqtt.server.c_str(), global->config.mqtt.port);
	client->connect();
}

void Mqtt::disconnect() {
	client->disconnect(true);
}

void Mqtt::request(MqttTopicList_e topic, const String data, uint8_t qos, bool retain) {
	const char *name = mqtt_topic_list_send[(uint8_t)topic].c_str();
	client->publish(name, qos, retain, data.c_str());
}

#endif