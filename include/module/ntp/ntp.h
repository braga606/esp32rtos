#pragma once
#ifndef MODULE_NTP_H
#define MODULE_NTP_H
#include <Arduino.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

class ServerTime {
	public:
		void init(UDP &udp, const char *poolServerName = "europe.pool.ntp.org", uint32_t timeOffset = 3600, uint32_t updateInterval = 60000);
		void begin();
		void tick();
		const unsigned long getTimestamp();

	private:
		NTPClient *ntp = nullptr;
		bool isInit = false;
		bool isBegin = false;
};

void ServerTime::init(UDP &udp, const char *poolServerName, uint32_t timeOffset, uint32_t updateInterval) {
	if (!isInit) {
		ntp = new NTPClient(udp, poolServerName, timeOffset, updateInterval);
		isInit = true;
	}
}

void ServerTime::begin() {
	if (!isBegin) {
		ntp->begin();
		isBegin = true;
	}
}

void ServerTime::tick() {
	if (isInit && isBegin) {
		ntp->update();
	}
}

const unsigned long ServerTime::getTimestamp() {
	if (isInit && isBegin) {
		return ntp->getEpochTime();
	}

	return 0;
}

#endif