#pragma once
#ifndef MODULE_DEBUG_H
#define MODULE_DEBUG_H
#include <Arduino.h>

#define DEBUG_FLAG 1

#if defined(DEBUG_FLAG) && DEBUG_FLAG == 1
	#define D_PRINT(...) Serial.print(__VA_ARGS__)
	#define D_PRINTLN(...) Serial.println(__VA_ARGS__)
	#define D_PRINTF(...) Serial.printf_P(__VA_ARGS__)
	#define D_PRINTFLN(...) Serial.printf_P(__VA_ARGS__); Serial.println(F(""))
#else
	#define D_PRINT(...)
	#define D_PRINTLN(...)
	#define D_PRINTF(...)
	#define D_PRINTFLN(...)
#endif

#endif