#pragma once
#ifndef MODULE_WIFI_STA_H
#define MODULE_WIFI_STA_H
#include <Arduino.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include "global/global.h"

class WifiSTA {
	public:
		void init(Global *global);
		void connect();
		void disconnect();

	private:
		bool isInit = false;
		Global *global = nullptr;
		WiFiMulti *wifi_multi = nullptr;
};

void WifiSTA::init(Global *global) {
	if (!isInit) {
		this->global = global;
		isInit = true;
	}
}

void WifiSTA::connect() {
	disconnect();

	WiFi.mode(WIFI_MODE_STA);

	if (wifi_multi != nullptr) {
		free(wifi_multi);
	}

	wifi_multi = new WiFiMulti();

	const WifiSTAAccesses_v accesses = global->config.wifi.sta.accesses;

	if (accesses.size() > 1) {
		for (WifiSTAAccess_t const access: accesses) {
			wifi_multi->addAP(
				access.ssid.c_str(),
				access.password.c_str()
			);
		}

		wifi_multi->run();
	}

	if (accesses.size() == 1) {
		const WifiSTAAccess_t access = accesses.front();
		WiFi.begin(access.ssid.c_str(), access.password.c_str());
	}
}

void WifiSTA::disconnect() {
	WiFi.disconnect(true, true);
	WiFi.setAutoReconnect(false);
}

#endif