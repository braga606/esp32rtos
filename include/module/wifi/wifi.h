#pragma once
#ifndef MODULE_WIFI_H
#define MODULE_WIFI_H
#include <Arduino.h>
#include <WiFi.h>
#include <Preferences.h>
#include "global/global.h"
#include "module/debug/debug.h"
#include "utils/json/json.h"
#include "module/wifi/sta/sta.h"
#include "module/wifi/ap/ap.h"

class Wifi {
	public:
		wifi_mode_t wifi_mode = WIFI_MODE_NULL;

		bool isInitWifiSTA = false;
		WifiSTA *wifi_sta = nullptr;

		bool isInitWifiAP = false;
		WifiAP *wifi_ap = nullptr;

		bool isAttachEvents = false;

		void init(Global *global);
		void connect();
		void disconnect();

		bool getAccesses();
		bool clearAccesses();
		bool saveAccesses(WifiSTAAccesses_v accesses);

	private:
		bool isInit = false;
		Global *global = nullptr;
		Preferences *storage = nullptr;
};

void Wifi::init(Global *global) {
	if (!isInit) {
		this->global = global;
		this->storage = new Preferences();

		if (getAccesses()) {
			if (!isInitWifiSTA) {
				global->mutex.serial->take();
				D_PRINTLN(F("Init WiFi STA mode"));
				global->mutex.serial->give();

				wifi_sta = new WifiSTA();
				wifi_mode = WIFI_MODE_STA;
				wifi_sta->init(global);
				isInitWifiSTA = true;
			}
		} else {
			if (!isInitWifiAP) {
				global->mutex.serial->take();
				D_PRINTLN(F("Init WiFi AP mode"));
				global->mutex.serial->give();

				wifi_ap = new WifiAP();
				wifi_mode = WIFI_MODE_AP;
				wifi_ap->init(global);

				isInitWifiAP = true;
			}
		}

		isInit = true;
	}
}

void Wifi::connect() {
	switch (wifi_mode) {
		case WIFI_MODE_STA:
			if (isInitWifiSTA) {
				wifi_sta->connect();
			}
			break;

		case WIFI_MODE_AP:
			if (isInitWifiAP) {
				wifi_ap->connect();
			}
			break;

		default: break;
	}
}

void Wifi::disconnect() {
	switch (wifi_mode) {
		case WIFI_MODE_STA:
			if (isInitWifiSTA) {
				wifi_sta->disconnect();
			}
			break;

		case WIFI_MODE_AP:
			if (isInitWifiAP) {
				wifi_ap->disconnect();
			}
			break;

		default: break;
	}
}

bool Wifi::clearAccesses() {
	storage->begin(global->config.storage.key.c_str(), false);
	return storage->remove(global->config.wifi.sta.storage_key.c_str());
}

bool Wifi::getAccesses() {
	bool result = false;

	storage->begin(global->config.storage.key.c_str(), true);

	const String data = storage->getString(global->config.wifi.sta.storage_key.c_str());

	if (data.length()) {
		const JsonParseResult_t json = Json::parse(data);

		if (json.success) {
			const JsonArray items = json.document.as<JsonArray>();

			if (items.size()) {
				WifiSTAAccesses_v accesses;

				for (JsonObject const item: items) {
					const String ssid = item[F("ssid")].as<String>();

					if (ssid.length()) {
						const String password = item[F("password")].as<String>();

						const WifiSTAAccess_t access = {
							.ssid = ssid,
							.password = password
						};

						accesses.push_back(access);
					}
				}

				if (accesses.size()) {
					global->config.wifi.sta.accesses.clear();

					global->config.wifi.sta.accesses.swap(accesses);

					if (global->config.wifi.sta.accesses.size()) {
						result = true;
					}
				}
			}
		}
	}

	storage->end();

	return result;
}

bool Wifi::saveAccesses(WifiSTAAccesses_v accesses) {
	uint8_t accesses_count = 0;

	const JsonCreateResult_t json = Json::create([&accesses, &accesses_count](DynamicJsonDocument &document) {
		const JsonArray array = document.to<JsonArray>();

		for (WifiSTAAccess_t const access: accesses) {
			if (access.ssid.length()) {
				const JsonObject item = array.createNestedObject();

				item[F("ssid")] = access.ssid;
				item[F("password")] = access.password;

				accesses_count++;
			}
		}
	});

	if (json.success && accesses_count) {
		storage->begin(global->config.storage.key.c_str(), false);
		storage->putString(global->config.wifi.sta.storage_key.c_str(), json.data);
		storage->end();

		return true;
	}

	return false;
}


#endif