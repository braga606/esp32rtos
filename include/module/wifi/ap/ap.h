#pragma once
#ifndef MODULE_WIFI_AP_H
#define MODULE_WIFI_AP_H
#include <Arduino.h>
#include <WiFi.h>
#include <DNSServer.h>
#include <ESPAsyncWebServer.h>
#include <AsyncJson.h>
#include "global/global.h"
#include "utils/filesystem/filesystem.h"
#include "module/wifi/scanner.h"

enum class WebsocketTopic_e {
	Networks = 1,
	Ap
};

enum class WebsocketStatus_e {
	Empty = 1,
	ToMany,
	NotToSave,
	Success
};

class WifiAP {
	public:
		void init(Global *global);
		void disconnect();
		void destroy();
		void connect();
		void keepAlive() const;

	private:
		bool isInit = false;
		Global *global = nullptr;
		AsyncWebServer *server = nullptr;
		AsyncWebSocket *ws = nullptr;
		DNSServer *dns = nullptr;
		void onWsMessage(AsyncWebSocketClient *client, const String data);
		void onWsNetworks(AsyncWebSocketClient *client);
		void onWsSaveAP(AsyncWebSocketClient *client, JsonArray &points);
		const String createWsResponse(const WebsocketTopic_e topic, const WebsocketStatus_e code, const String data = "") const;
};

void WifiAP::init(Global *global) {
	if (!isInit) {
		this->global = global;
		this->server = new AsyncWebServer(this->global->config.wifi.ap.server.server_port);
		this->ws = new AsyncWebSocket(this->global->config.wifi.ap.websocket.url);
		this->dns = new DNSServer();
		isInit = true;
	}
}

void WifiAP::destroy() {
	if (isInit) {
		disconnect();

		global = nullptr;
		free(server);
		free(dns);
		free(ws);

		isInit = false;
	}
}

void WifiAP::disconnect() {
	if (isInit) {
		ws->closeAll();
		server->end();
		dns->stop();
		WiFi.disconnect(true, true);
		WiFi.mode(WIFI_MODE_NULL);
	}
}

void WifiAP::connect() {
	if (!isInit) {
		return;
	}

	if (!FileSystem::begin()) {
		D_PRINTLN(F("Failed begin SPIFFS"));
		return;
	}

	WiFi.disconnect(true, true);

	WiFi.mode(WIFI_MODE_AP);

	WiFi.enableAP(true);

	delay(1000);

	WiFi.softAPConfig(
		global->config.wifi.ap.server.ip,
		global->config.wifi.ap.server.ip,
		global->config.wifi.ap.server.subnet
	);

	delay(1000);

	WiFi.softAP(
		global->config.wifi.ap.ssid.c_str(),
		global->config.wifi.ap.password.c_str()
	);

	delay(1000);

	D_PRINTFLN(PSTR("Wifi AP IP: %s"), WiFi.softAPIP().toString().c_str());

	dns->start(global->config.wifi.ap.server.dns_port, "*", global->config.wifi.ap.server.ip);

	ws->onEvent([this](AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len) {
		D_PRINTLN(F("WS event"));

		switch (type) {
			case WS_EVT_CONNECT:
				D_PRINTLN(F("WS client connected"));
			break;

			case WS_EVT_DISCONNECT:
				D_PRINTLN(F("WS client disconnected"));
			break;

			case WS_EVT_PONG:
				D_PRINTLN(F("WS client pong"));
			break;

			case WS_EVT_ERROR:
				D_PRINTLN(F("WS client error"));
			break;

			case WS_EVT_DATA:
				data[len] = '\0';
				const String payload = (const char *)data;
				onWsMessage(client, payload);
			break;
		}
	});

	server->onNotFound([this](AsyncWebServerRequest *request) {
		const String url = request->url();

		if (url == F("/")) {
			request->send(SPIFFS, F("/index.html"), F("text/html"));
		} else if (url.indexOf(F("/index.html")) >= 0) {
			request->redirect(F("/"));
		} else {
			if (FileSystem::exist(url)) {
				request->send(SPIFFS, url);
			} else {
				request->redirect(F("/"));
			}
		}
	});

	server->addHandler(ws);

	server->begin();

	delay(1000);

	global->events.group->set(global->events.wifi.ap.connected);
}

const String WifiAP::createWsResponse(const WebsocketTopic_e topic, const WebsocketStatus_e code, const String data) const {
	const String statusText = WebsocketStatus_e::Success == code ? "true" : "false";
	const String statusJson = String("\"status\":") + statusText;
	const String topicJson = String("\"topic\":") + static_cast<uint8_t>(topic);
	const String codeJson = String("\"code\":") + static_cast<uint8_t>(code);
	const String dataText = data.length() ? data : String("null");
	const String dataJson = String("\"data\":") + dataText;
	return String("{") + statusJson + String(",") + topicJson + String(",") + codeJson + String(",") + dataJson + String("}");
}

void WifiAP::onWsNetworks(AsyncWebSocketClient *client) {
	D_PRINTLN(F("WS networks topic"));

	WifiScanner::scanAsync([this, client](const String networks) {
		const String response = createWsResponse(
			WebsocketTopic_e::Networks,
			WebsocketStatus_e::Success,
			networks
		);

		client->text(response);
	});
}

void WifiAP::onWsSaveAP(AsyncWebSocketClient *client, JsonArray &points) {
	D_PRINTLN(F("WS save ap topic"));

	if (points.size() <= 0) {
		return client->text(createWsResponse(
			WebsocketTopic_e::Ap,
			WebsocketStatus_e::Empty
		));
	}

	if (points.size() > global->config.wifi.sta.max_accsesses) {
		return client->text(createWsResponse(
			WebsocketTopic_e::Ap,
			WebsocketStatus_e::ToMany
		));
	}

	WifiSTAAccesses_v accesses;

	for (JsonObject const point: points) {
		const String ssid = point[F("ssid")].as<String>();

		if (ssid.length()) {
			const String password = point[F("password")].as<String>();

			const WifiSTAAccess_t access = {
				.ssid = ssid,
				.password = password
			};

			D_PRINTFLN(PSTR("SSID: %s | PASSWORD: %s"), ssid.c_str(), password.c_str());

			accesses.push_back(access);
		}
	}

	if (!accesses.size()) {
		return client->text(createWsResponse(
			WebsocketTopic_e::Ap,
			WebsocketStatus_e::NotToSave
		));
	}

	global->config.wifi.sta.accesses.swap(accesses);

	client->text(createWsResponse(
		WebsocketTopic_e::Ap,
		WebsocketStatus_e::Success
	));

	global->events.group->set(global->events.wifi.ap.save_accesses);
}

void WifiAP::onWsMessage(AsyncWebSocketClient *client, const String data) {
	D_PRINTFLN(PSTR("WS message: %s"), data.c_str());

	if (!data.length()) {
		return;
	}

	JsonParseResult_t json = Json::parse(data);

	if (!json.success) {
		return;
	}

	const uint8_t topic = json.document[F("topic")].as<uint8_t>();
	JsonArray points = json.document[F("data")].as<JsonArray>();

	if (topic == 0) {
		D_PRINTLN(F("Invalid topic"));
		return;
	}

	switch ((WebsocketTopic_e)topic) {
		case WebsocketTopic_e::Networks:
			onWsNetworks(client);
		break;

		case WebsocketTopic_e::Ap:
			onWsSaveAP(client, points);
		break;

		default:
			D_PRINTFLN(PSTR("Not find topic: %d"), topic);
		break;
	}
}

void WifiAP::keepAlive() const {
	if (isInit) {
		dns->processNextRequest();
		ws->cleanupClients(global->config.wifi.ap.websocket.max_client);
	}
}

#endif