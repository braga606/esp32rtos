#pragma once
#ifndef MODULE_WIFI_SCANNER_H
#define MODULE_WIFI_SCANNER_H
#include <Arduino.h>
#include <WiFi.h>
#include <Ticker.h>
#include "utils/json/json.h"
#include "global/global.h"

namespace WifiScanner {
	Ticker ticker;
	String json_cache;
	String json_empty = F("[]");
	WifiScannerOnCompletes_v onCompleteStack;
	WifiScannerOnFaileds_v onFailedStack;
	unsigned long prev_millis = 0;
	uint32_t scan_max_interval = 20000;
	uint16_t update_interval = 200;
	bool is_start = false;

	void clearCallbackStack() {
		onCompleteStack.clear();
		onFailedStack.clear();
	}

	void callOnComplete(const String json) {
		if (onCompleteStack.size() == 0) {
			return;
		}

		for (WifiScannerOnComplete_t onComplete: onCompleteStack) {
			onComplete(json);
		}
	}

	void callOnFailed(const String cache) {
		if (onFailedStack.size() == 0) {
			return;
		}

		for (WifiScannerOnFailed_t onFailed: onFailedStack) {
			onFailed(cache);
		}
	}

	void startAsync() {
		if (is_start) {
			return;
		}

		is_start = true;

		WiFi.scanDelete();

		WiFi.scanNetworks(true, false);

		ticker.attach_ms(update_interval, []() {
			const int16_t result = WiFi.scanComplete();

			if (result >= 0) {
				ticker.detach();

				JsonCreateResult_t json = Json::create([result](DynamicJsonDocument &document) {
					const JsonArray array = document.to<JsonArray>();

					for (int16_t i = 0; i < result; i++) {
						const JsonObject object = array.createNestedObject();

						object[F("ssid")] = WiFi.SSID(i);
						object[F("rssi")] = WiFi.RSSI(i);
						object[F("chanel")] = WiFi.channel(i);
						object[F("encryption")] = static_cast<uint8_t>(WiFi.encryptionType(i));
						object[F("mac")] = WiFi.BSSIDstr(i);
					}
				}, JsonCreateMode_e::Simple, 6144);

				WiFi.scanDelete();

				is_start = false;
				prev_millis = millis();

				if (json.success) {
					json_cache = json.data;
					callOnComplete(json_cache);
				} else {
					callOnFailed(json_cache.length() ? json_cache : String(F("")));
				}

				clearCallbackStack();
			} else if (result == WIFI_SCAN_FAILED) {
				WiFi.scanDelete();
				ticker.detach();

				is_start = false;
				prev_millis = millis();

				callOnFailed(json_cache.length() ? json_cache : String(F("")));

				clearCallbackStack();
			}
		});
	}

	void scanAsync(WifiScannerOnComplete_t onComplete, WifiScannerOnFailed_t onFailed = NULL) {
		if (millis() - prev_millis < scan_max_interval && json_cache.length() > 0) {
			onComplete(json_cache);

			return;
		}

		if (onCompleteStack.size() == 0) {
			startAsync();
		}

		onCompleteStack.push_back(onComplete);

		if (onFailed != NULL) {
			onFailedStack.push_back(onFailed);
		}
	}
}

#endif