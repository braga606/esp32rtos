#pragma once
#ifndef GLOBAL_H
#define GLOBAL_H
#include <Arduino.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include "module/debug/debug.h"
#include "rtos/mutex.h"
#include "rtos/queue.h"
#include "rtos/events.h"
#include "rtos/timer.h"
#include "sensors/dht/dht.h"
#include "sensors/sensors.h"
#include "utils/json/json.h"

typedef struct {
	String ssid;
	String password;
} WifiSTAAccess_t;

typedef std::vector<WifiSTAAccess_t> WifiSTAAccesses_v;

typedef struct {
	String ssid;
	int32_t rssi;
	int32_t chanel;
	wifi_auth_mode_t encryption;
	String mac;
} WifiNetwork_t;

typedef std::vector<WifiNetwork_t> WifiNetworks_v;

typedef std::function<void(const String json)> WifiScannerOnComplete_t;

typedef std::vector<WifiScannerOnComplete_t> WifiScannerOnCompletes_v;

typedef std::function<void(const String json)> WifiScannerOnFailed_t;

typedef std::vector<WifiScannerOnFailed_t> WifiScannerOnFaileds_v;

typedef struct {
	SensorDht *dht = nullptr;
} GlobalSensors_t;

typedef struct {
	struct Wifi {
		Timer *reconnect = nullptr;
	} wifi;

	struct Mqtt {
		Timer *reconnect = nullptr;
	} mqtt;
} GlobalTimers_t;

typedef struct {
	Mutex *serial = nullptr;
} GlobalMutex_t;

typedef struct {
	QueueDynamic<SensorData_t, sizeof(SensorData_t), 20> *sensors = nullptr;
} GlobalQueue_t;

typedef struct {
	String id;

	uint32_t time = 0;

	struct Serial {
		uint32_t baud = 115200;
	} serial;

	struct Mqtt {
		String user = "BRAGA";
		String password = "0674384987";
		String server = "192.168.0.103";
		uint16_t port = 1883;
		uint32_t reconnectInMs = 2000;
		uint8_t max_reconnect_try = 10;
	} mqtt;

	struct Storage {
		String key = "nvs";
	} storage;

	struct Wifi {
		struct Sta {
			WifiSTAAccesses_v accesses;
			uint8_t max_accsesses = 4;
			uint32_t reconnectInMs = 2000;
			uint8_t max_reconnect_try = 10;
			String storage_key = "wifi_sta";
		} sta;

		struct Ap {
			String ssid = "ESP32";
			String password = "12345678";

			struct Server {
				IPAddress ip = IPAddress(192, 168, 0, 100);
				IPAddress subnet = IPAddress(255, 255, 255, 0);
				uint16_t dns_port = 53;
				uint16_t server_port = 80;
			} server;

			struct Websocket {
				String url = "/ws";
				uint8_t max_client = 4;
			} websocket;
		} ap;
	} wifi;
} GlobalConfig_t;

typedef struct {
	WiFiUDP udp;
} GlobalNetwork_t;

typedef struct {
	Events *group = nullptr;

	struct Wifi {
		struct Sta {
			uint8_t connected = BIT0;
			uint8_t disconnected = BIT1;
		} sta;

		struct Ap {
			uint8_t connected = BIT2;
			uint8_t disconnected = BIT3;
			uint8_t save_accesses = BIT4;
		} ap;
	} wifi;

	struct Mqtt {
		uint8_t connected = BIT5;
		uint8_t disconnected = BIT6;
	} mqtt;
} GlobalEventGroup_t;

typedef struct {
	struct Wifi {
		struct Sta {
			TaskHandle_t connected;
			TaskHandle_t disconnected;
		} sta;

		struct Ap {
			TaskHandle_t connected;
			TaskHandle_t disconnected;
			TaskHandle_t save_accesses;
			TaskHandle_t keep_alive;
		} ap;
	} wifi;

	struct Mqtt {
		TaskHandle_t connected;
		TaskHandle_t disconnected;
		TaskHandle_t send;
	} mqtt;

	struct Sensors {
		TaskHandle_t dht;
	} sensors;

	TaskHandle_t time;
} GlobalTaskHandler_t;

class Global {
	public:
		void init();
		String getClientId();
		String getInfo();

		GlobalConfig_t config;
		GlobalSensors_t sensors;
		GlobalMutex_t mutex;
		GlobalQueue_t queue;
		GlobalNetwork_t network;
		GlobalEventGroup_t events;
		GlobalTaskHandler_t tasks;
		GlobalTimers_t timers;
};

void Global::init() {
	D_PRINTLN(F("Start Global::init"));

	config.id = getClientId();
	D_PRINTFLN(PSTR("Set clientId: %s"), config.id.c_str());

	#if defined(DEBUG_FLAG) && DEBUG_FLAG == 1
	mutex.serial = new Mutex();
	D_PRINTLN(F("Create serial mutex"));
	#else
	mutex.serial = new FakeMutex();
	#endif

	sensors.dht = new SensorDht(GPIO_NUM_17, 22);
	sensors.dht->init();
	D_PRINTLN(F("Create sensor dht"));

	queue.sensors = new QueueDynamic<SensorData_t, sizeof(SensorData_t), 20>;
	D_PRINTLN(F("Create sensors queue"));

	timers.wifi.reconnect = new Timer();
	D_PRINTLN(F("Create wifi reconnect timer"));

	timers.mqtt.reconnect = new Timer();
	D_PRINTLN(F("Create mqtt reconnect timer"));

	events.group = new Events();
	D_PRINTLN(F("Created events group"));
}

String Global::getClientId() {
	String id;

	uint8_t mac[6];
	WiFi.macAddress(mac);

	for (uint8_t i = 0; i < 6; ++i) {
		id += String(mac[i], HEX);
	}

	return id;
}

String Global::getInfo() {
	JsonCreateResult_t json = Json::create([](DynamicJsonDocument &document) {
		document[F("free_heap")] = ESP.getFreeHeap();
		document[F("free_sketch_space")] = ESP.getFreeSketchSpace();
	});

	if (json.success) {
		return json.data;
	}

	return String();
}

#endif