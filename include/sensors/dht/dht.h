#pragma once
#ifndef SENSORS_DHT_H
#define SENSORS_DHT_H
#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include "utils/json/json.h"
#include "sensors/sensors.h"

typedef struct {
	float temperature;
	float humudity;
	float heat;
} SensorDhtData_t;

enum class SensorDhtError_e { None = 0, Unkown, isNaN };

typedef struct {
	bool success = false;
	SensorDhtError_e error;
	SensorDhtData_t data;
} SensorDhtResult_t;

class SensorDht: public Sensor {
	public:
		SensorDht(uint8_t pin, uint8_t type = 22) : dht(pin, type) {
			this->pin = pin;
			this->type = type;
		}

		void init() {
			if (!isInit) {
				dht.begin();
				isInit = true;
			}
		}

		SensorDhtResult_t read() {
			SensorDhtResult_t result;
			result.success = false;
			result.error = SensorDhtError_e::None;

			const float temperature = dht.readTemperature(false);

			if (isnan(temperature)) {
				result.error = SensorDhtError_e::isNaN;
				return result;
			} else {
				result.data.temperature = temperature;
			}

			const float humudity = dht.readHumidity();

			if (isnan(humudity)) {
				result.error = SensorDhtError_e::isNaN;
				return result;
			} else {
				result.data.humudity = humudity;
			}

			const float heat = dht.computeHeatIndex(temperature, humudity, false);

			if (isnan(heat)) {
				result.error = SensorDhtError_e::isNaN;
				return result;
			} else {
				result.data.heat = heat;
			}

			result.success = true;

			return result;
		}

		String toJSON() {
			const SensorDhtResult_t sensor = read();

			if (!sensor.success) {
				return String();
			}

			JsonCreateResult_t json = Json::create([sensor](DynamicJsonDocument &document) {
				document[F("temperature")] = sensor.data.temperature;
				document[F("humudity")] = sensor.data.humudity;
				document[F("heat")] = sensor.data.heat;
			});

			if (!json.success) {
				return String();
			}

			return json.data;
		}

	private:
		DHT dht;
		bool isInit = false;
		uint8_t pin;
		uint8_t type;
};

#endif