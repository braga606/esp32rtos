#pragma once
#ifndef SENSORS_H
#define SENSORS_H
#include <Arduino.h>

typedef struct {
	const char *sensor;
	const char *data;
	unsigned long createdAt;
} SensorData_t;

class Sensor {
	public:
		virtual String toJSON() = 0;
};

#endif