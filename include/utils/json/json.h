#pragma once
#ifndef UTILS_JSON_H
#define UTILS_JSON_H
#include <ArduinoJson.h>
#include <functional>

#define DEFAULT_JSON_BUFFER_SIZE 2048

typedef std::function<void(DynamicJsonDocument &document)> JsonCreateCallback_t;

enum class JsonCreateMode_e { Simple = 0, Pretty };

typedef struct {
	bool success;
	String data;
	String error;
} JsonCreateResult_t;

typedef struct {
	bool success;
	JsonVariant document;
	String error;
} JsonParseResult_t;

class Json {
   public:
    static JsonCreateResult_t create(JsonCreateCallback_t callback, JsonCreateMode_e mode = JsonCreateMode_e::Simple, const uint32_t buffer = DEFAULT_JSON_BUFFER_SIZE) {
		JsonCreateResult_t result;
		DynamicJsonDocument document(buffer);
		callback(document);
		String json;

		switch (mode) {
			case JsonCreateMode_e::Simple:
				serializeJson(document, json);
				break;
			case JsonCreateMode_e::Pretty:
				serializeJsonPretty(document, json);
				break;
		}

		result.success = true;
		result.data = json;
		return result;
	}

    static JsonParseResult_t parse(const String json, const uint32_t buffer = DEFAULT_JSON_BUFFER_SIZE) {
		JsonParseResult_t result;
		DynamicJsonDocument document(buffer);
		DeserializationError error = deserializeJson(document, json);

		if (error) {
			result.success = false;
			result.error = String(error.c_str());
		} else {
			result.success = true;
			result.document = document.as<JsonVariant>();
		}

		return result;
	}
};

#endif