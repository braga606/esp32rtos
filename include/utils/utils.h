#pragma once
#ifndef UTILS_H
#define UTILS_H
#include <Arduino.h>
#include "module/debug/debug.h"

void initDebugSerial(const uint32_t baud) {
	#if defined(DEBUG_FLAG) && DEBUG_FLAG == 1
	Serial.begin(baud);
	delay(1000);
	while (!Serial) continue;
	D_PRINTLN(F(""));
	D_PRINTLN(F("Start Serial Debug"));
	#endif
}

bool isValidTimestamp(const uint32_t timestamp) {
	return timestamp > 1581253300;
}

#endif