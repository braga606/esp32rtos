#pragma once
#ifndef UTILS_FS_H
#define UTILS_FS_H
#include <Arduino.h>
#include <SPIFFS.h>

namespace FileSystem {
	bool is_begin = false;

	bool begin() {
		if (is_begin) {
			return true;
		}

		if (SPIFFS.begin(true)) {
			is_begin = true;
			return true;
		}

		return false;
	}

	void end() {
		if (is_begin) {
			SPIFFS.end();
			is_begin = false;
		}
	}

	bool exist(const String path) {
		if (!begin()) {
			return false;
		}

		return SPIFFS.exists(path);
	}

	String read(const String path) {
		String result;

		if (!begin()) {
			return result;
		}

		File file = SPIFFS.open(path.c_str(), FILE_READ);

		while (file.available()) {
			result += char(file.read());
		}

		file.close();

		return result;
	}
}

#endif