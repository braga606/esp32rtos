#include <Arduino.h>
#include <FreeRTOS.h>
#include "global/global.h"
#include "utils/utils.h"
#include "rtos/task.h"
#include "module/debug/debug.h"
#include "module/ntp/ntp.h"
#include "module/wifi/wifi.h"
#include "sensors/sensors.h"
#include "module/mqtt/mqtt.h"

#include "module/wifi/ap/ap.h"
#include "utils/filesystem/filesystem.h"

Global global;
Wifi wifi;
Mqtt mqtt;
ServerTime server_time;

FileSystem filesystem;
WifiAP wifi_ap;

void suspendWifiDependTasks();
void resumeWifiDependTasks();
void timerCallbackWifiReconnect(TimerHandle_t xTimer);
void startWifiSTA();
void taskWifiStaConnected(void *params);
void taskWifiStaDisconnected(void *params);
void initWifi();

void suspendMqttDependTasks();
void resumeMqttDependTasks();
void taskMqttConnected();
void taskMqttDisconnected();
void timerCallbackMqttReconnect(TimerHandle_t xTimer);
void taskMqttConnected(void *params);
void taskMqttDisconnected(void *params);
void taskMqttQueueSend(void *params);

void taskServerTimeTick(void *params);

void taskSensorDhtRead(void *params);

void suspendWifiDependTasks() {
	global.mutex.serial->take();
	D_PRINTLN(F("Suspend wifi depenend tasks"));
	global.mutex.serial->give();

	mqtt.disconnect();
	global.timers.mqtt.reconnect->stop();

	Task::suspend(global.tasks.time);
	Task::suspend(global.tasks.mqtt.connected);
	Task::suspend(global.tasks.mqtt.disconnected);
	Task::suspend(global.tasks.mqtt.send);
}

void resumeWifiDependTasks() {
	global.mutex.serial->take();
	D_PRINTLN(F("Resume wifi depenend tasks"));
	global.mutex.serial->give();

	server_time.begin();

	global.timers.mqtt.reconnect->init(
		timerCallbackMqttReconnect,
		"TIMER [MQTT RECONNECT]",
		2000
	);

	mqtt.connect();

	Task::resume(global.tasks.time);
	Task::resume(global.tasks.mqtt.connected);
	Task::resume(global.tasks.mqtt.disconnected);
	Task::resume(global.tasks.mqtt.send);
}

void suspendMqttDependTasks() {
	global.mutex.serial->take();
	D_PRINTLN(F("Suspend mqtt depenend tasks"));
	global.mutex.serial->give();

	Task::suspend(global.tasks.sensors.dht);
	Task::suspend(global.tasks.mqtt.send);
}

void resumeMqttDependTasks() {
	global.mutex.serial->take();
	D_PRINTLN(F("Resume mqtt depenend tasks"));
	global.mutex.serial->give();

	Task::resume(global.tasks.sensors.dht);
	Task::resume(global.tasks.mqtt.send);
}

void taskSensorDhtRead(void *params) {
	while (true) {
		const String json = global.sensors.dht->toJSON();

		if (json.length() > 0 && isValidTimestamp(global.config.time)) {
			const SensorData_t data = {
				.sensor = "DHT",
				.data = json.c_str(),
				.createdAt = global.config.time
			};

			global.queue.sensors->send(data);
		} else {
			global.mutex.serial->take();
			D_PRINTLN(F("Failed read dht sensor"));
			global.mutex.serial->give();
		}

		Task::delay(2000);
	}
}

void taskMqttQueueSend(void *params) {
	while (true) {
		SensorData_t result;
		global.queue.sensors->receive(&result);

		global.mutex.serial->take();
		D_PRINTFLN(
			PSTR("Sensor [%s] queue recive: %s at %lu"),
			result.sensor,
			result.data,
			result.createdAt
		);
		global.mutex.serial->give();

		mqtt.request(MqttTopicList_e::SensorDht, result.data);
	}
}

void taskServerTimeTick(void *params) {
	while (true) {
		server_time.tick();
		global.config.time = server_time.getTimestamp();
		Task::delay(1000);
	}
}

void taskWifiStaConnected(void *params) {
	while (true) {
		global.events.group->wait(global.events.wifi.sta.connected);

		global.mutex.serial->take();
		D_PRINTLN(F("Wifi conected success"));
		global.mutex.serial->give();

		resumeWifiDependTasks();

		if (global.timers.wifi.reconnect->isActive) {
			global.mutex.serial->take();
			D_PRINTLN(F("Wifi reconnect timer stop"));
			global.mutex.serial->give();

			global.timers.wifi.reconnect->stop();
		}
	}
}

void taskWifiStaDisconnected(void *params) {
	while (true) {
		global.events.group->wait(global.events.wifi.sta.disconnected);

		global.mutex.serial->take();
		D_PRINTLN(F("Wifi disconnected"));
		global.mutex.serial->give();

		suspendWifiDependTasks();

		if (!global.timers.wifi.reconnect->isActive) {
			global.mutex.serial->take();
			D_PRINTLN(F("Wifi reconnect timer start"));
			global.mutex.serial->give();

			global.timers.wifi.reconnect->start();
		}
	}
}

void taskMqttConnected(void *params) {
	while (true) {
		global.events.group->wait(global.events.mqtt.connected);

		global.mutex.serial->take();
		D_PRINTLN(F("MQTT connected"));
		global.mutex.serial->give();

		global.timers.mqtt.reconnect->clearCount();

		if (global.timers.mqtt.reconnect->isActive) {
			global.mutex.serial->take();
			D_PRINTLN(F("Stop MQTT reconnect timer"));
			global.mutex.serial->give();

			global.timers.mqtt.reconnect->stop();
		}

		resumeMqttDependTasks();
	}
}

void taskMqttDisconnected(void *params) {
	while (true) {
		global.events.group->wait(global.events.mqtt.disconnected);

		global.mutex.serial->take();
		D_PRINTLN(F("MQTT disconnected"));
		global.mutex.serial->give();

		if (!global.timers.mqtt.reconnect->isActive) {
			global.mutex.serial->take();
			D_PRINTLN(F("Start MQTT reconnect timer"));
			global.mutex.serial->give();

			global.timers.mqtt.reconnect->start(false);
		}

		suspendMqttDependTasks();
	}
}

void timerCallbackWifiReconnect(TimerHandle_t xTimer) {
	global.mutex.serial->take();
	D_PRINTFLN(PSTR("WIFI reconnecting ... %d"), global.timers.wifi.reconnect->count);
	global.mutex.serial->give();

	wifi.wifi_sta->connect();

	global.timers.wifi.reconnect->incrementCount();
}

void timerCallbackMqttReconnect(TimerHandle_t xTimer) {
	global.mutex.serial->take();
	D_PRINTFLN(PSTR("MQTT reconnecting ... %d"), global.timers.mqtt.reconnect->count);
	global.mutex.serial->give();

	mqtt.connect();

	global.timers.mqtt.reconnect->incrementCount();
	global.timers.mqtt.reconnect->stop(false);
}

void attachWifiEvents() {
	if (!wifi.isAttachEvents) {
		WiFi.onEvent([](WiFiEvent_t event) {
			switch (event) {
				case SYSTEM_EVENT_STA_GOT_IP:
					global.events.group->set(global.events.wifi.sta.connected);
					break;
				case SYSTEM_EVENT_STA_DISCONNECTED:
					global.events.group->set(global.events.wifi.sta.disconnected);
					break;
				case SYSTEM_EVENT_AP_START:
					global.events.group->set(global.events.wifi.ap.connected);
					break;
				case SYSTEM_EVENT_AP_STOP:
					global.events.group->set(global.events.wifi.ap.disconnected);
					break;
				default: break;
			}
		});

		wifi.isAttachEvents = true;
	}
}

void startWifiSTA() {
	global.timers.wifi.reconnect->init(
		timerCallbackWifiReconnect,
		"TIMER [WIFI RECONNECT]",
		global.config.wifi.sta.reconnectInMs
	);

	wifi.wifi_sta->connect();
}

void setDefaultStaAccesses() {
	WifiSTAAccesses_v accesses;

	const WifiSTAAccess_t access = {
		.ssid = "BRAGA",
		.password = "0674384987"
	};

	accesses.push_back(access);

	if (wifi.saveAccesses(accesses)) {
		D_PRINTLN(F("Saved default wifi accesses"));
	} else {
		D_PRINTLN(F("Failed save default wifi accesses"));
	}
}

void initWifi() {
	attachWifiEvents();

	switch (wifi.wifi_mode) {
		case WIFI_MODE_STA:
			global.mutex.serial->take();
			D_PRINTLN(F("Start wifi STA mode"));
			global.mutex.serial->give();
			startWifiSTA();
			break;
		default: break;
	}
}

void setup() {
	initDebugSerial(global.config.serial.baud);

	global.init();
	// wifi.init(&global);
	// mqtt.init(&global);
	// server_time.init(global.network.udp);

	filesystem.init(&global);

	if (filesystem.begin()) {
		D_PRINTLN(F("Success fs begin"));

		// const String html = filesystem.read("/index.html");
		// D_PRINTLN(html);

		wifi_ap.init(&global);
		wifi_ap.connect();
	}

	// initWifi();

	// setDefaultStaAccesses();

	// Task::create(
	// 	taskWifiStaConnected,
	// 	"TASK [WIFI STA: CONNECTED]",
	// 	&global.tasks.wifi.connected,
	// 	2048
	// );

	// Task::create(
	// 	taskWifiStaDisconnected,
	// 	"TASK [WIFI STA: DISCONNECTED]",
	// 	&global.tasks.wifi.disconnected,
	// 	2048
	// );

	// Task::create(
	// 	taskMqttConnected,
	// 	"TASK [MQTT: CONNECTED]",
	// 	&global.tasks.mqtt.connected,
	// 	2048
	// );

	// Task::create(
	// 	taskMqttDisconnected,
	// 	"TASK [MQTT: DISCONNECTED]",
	// 	&global.tasks.mqtt.disconnected,
	// 	2048
	// );

	// Task::create(
	// 	taskServerTimeTick,
	// 	"TASK [NTP TIME TICK]",
	// 	&global.tasks.time,
	// 	4096
	// );

	// Task::create(
	// 	taskSensorDhtRead,
	// 	"TASK [DHT READ]",
	// 	&global.tasks.sensors.dht,
	// 	4096
	// );

	// Task::create(
	// 	taskMqttQueueSend,
	// 	"TASK [MQTT SEND]",
	// 	&global.tasks.mqtt.send,
	// 	4096
	// );

	// Task::suspend(global.tasks.time);
	// Task::suspend(global.tasks.sensors.dht);
	// Task::suspend(global.tasks.mqtt.connected);
	// Task::suspend(global.tasks.mqtt.disconnected);
	// Task::suspend(global.tasks.mqtt.send);

	D_PRINTFLN(PSTR("Setup end. Global info: %s\n"), global.getInfo().c_str());
}

void loop() {
	// wifi_ap.keepAlive();
}