#pragma once
#ifndef MODULE_WIFI_AP_H
#define MODULE_WIFI_AP_H
#include <Arduino.h>
#include <WiFi.h>
#include <DNSServer.h>
#include <WebServer.h>
#include <ESPAsyncWebServer.h>
#include <AsyncJson.h>
#include "global/global.h"
#include "utils/filesystem/filesystem.h"
#include "utils/json/json.h"

class WifiAP {
	public:
		void init(Global *global);
		void disconnect();
		void destroy();
		void connect();
		void keepAlive();

	private:
		bool isInit = false;
		String page;
		Global *global = nullptr;
		// WebServer *server = nullptr;
		AsyncWebServer *server = nullptr;
		DNSServer *dns = nullptr;
};

void WifiAP::init(Global *global) {
	if (!isInit) {
		this->global = global;
		// this->server = new WebServer(this->global->config.wifi.ap.server_port);
		this->server = new AsyncWebServer(this->global->config.wifi.ap.server_port);
		this->dns = new DNSServer();
		isInit = true;
	}
}

void WifiAP::destroy() {
	if (isInit) {
		disconnect();

		global = nullptr;
		free(server);
		free(dns);

		isInit = false;
	}
}

void WifiAP::disconnect() {
	if (isInit) {
		server->end();
		// server->close();
		dns->stop();
		WiFi.disconnect(true, true);
		WiFi.mode(WIFI_MODE_NULL);
	}
}

void WifiAP::connect() {
	if (!isInit) {
		return;
	}

	if (!FileSystem::begin()) {
		D_PRINTLN(F("Failed begin SPIFFS"));
		return;
	}

	if (!page.length()) {
		const String html = FileSystem::read("/index.html");

		if (html.length()) {
			page = html;
		} else {
			D_PRINTLN(F("Empty AP html page"));
			return;
		}
	}

	WiFi.disconnect(true, true);

	WiFi.mode(WIFI_MODE_AP);

	WiFi.enableAP(true);

	delay(1000);

	WiFi.softAPConfig(
		global->config.wifi.ap.ip,
		global->config.wifi.ap.ip,
		global->config.wifi.ap.subnet
	);

	delay(1000);

	WiFi.softAP(
		global->config.wifi.ap.ssid.c_str(),
		global->config.wifi.ap.password.c_str()
	);

	delay(1000);

	D_PRINTFLN(PSTR("Wifi AP IP: %s"), WiFi.softAPIP().toString().c_str());

	dns->start(
		global->config.wifi.ap.dns_port,
		"*",
		global->config.wifi.ap.ip
	);

	// server->on("/ap", HTTP_POST, [this]() {
	// 	const String plain = server->arg("plain");

	// 	if (plain.length()) {
	// 		const JsonParseResult_t json = Json::parse(plain);

	// 		if (json.success) {
	// 			const JsonArray accesses = json.document.as<JsonArray>();

	// 			if (accesses.size()) {
	// 				const uint8_t accesses_max_count = global->config.wifi.sta.max_accsesses;

	// 				WifiSTAAccesses_v points;

	// 				for (JsonObject const access: accesses) {
	// 					const String ssid = access["ssid"].as<String>();

	// 					if (ssid.length()) {
	// 						const String password = access["password"].as<String>();

	// 						const WifiSTAAccess_t point = {
	// 							.ssid = ssid,
	// 							.password = password
	// 						};

	// 						if (points.size() < accesses_max_count) {
	// 							D_PRINTFLN(PSTR("Accept wifi ap access. SSID: %s | PASSWORD: %s"), ssid.c_str(), password.c_str());
	// 							points.push_back(point);
	// 						}
	// 					}
	// 				}

	// 				if (points.size() > 0) {
	// 					global->config.wifi.sta.accesses.swap(points);
	// 					global->events.group->set(global->events.wifi.ap.save_accesses);
	// 					server->send(200, "application/json", "{ success: true, error: null, result: "+ plain +" }");
	// 				} else {
	// 					server->send(400, "application/json", F("{ success: false, error: \"Empty ssid access\", result: null }"));
	// 				}
	// 			} else {
	// 				server->send(400, "application/json", F("{ success: false, error: \"Empty accesses array\", result: null }"));
	// 			}
	// 		} else {
	// 			server->send(400, "application/json", "{ success: false, error: "+ json.error +", result: null }");
	// 		}
	// 	} else {
	// 		server->send(400, "application/json", F("{ success: false, error: \"Empty body\", result: null }"));
	// 	}
	// });

	AsyncCallbackJsonWebHandler* ap_handler = new AsyncCallbackJsonWebHandler("/ap", [this](AsyncWebServerRequest *request, JsonVariant &json) {
		const JsonArray accesses = json.as<JsonArray>();

		if (accesses.size()) {
			const uint8_t accesses_max_count = global->config.wifi.sta.max_accsesses;

			WifiSTAAccesses_v points;

			for (JsonObject const access: accesses) {
				const String ssid = access["ssid"].as<String>();

				if (ssid.length() >= 3) {
					const String password = access["password"].as<String>();

					const WifiSTAAccess_t point = {
						.ssid = ssid,
						.password = password
					};

					if (points.size() < accesses_max_count) {
						D_PRINTFLN(PSTR("Accept wifi ap access. SSID: %s | PASSWORD: %s"), ssid.c_str(), password.c_str());
						points.push_back(point);
					}
				}
			}

			if (points.size()) {
				global->config.wifi.sta.accesses.swap(points);
				global->events.group->set(global->events.wifi.ap.save_accesses);
				request->send(200, "application/json", "{ success: true, error: null }");
				return;
			}

			request->send(400, "application/json", "{ success: false, error: true }");
		}
	});

	server->addHandler(ap_handler);

	server->onNotFound([this](AsyncWebServerRequest *request) {
		const String url = request->url();

		if (url == "/") {
			request->send(200, "text/html", page);
		} else if (url.indexOf("/index.html") >= 0) {
			request->redirect("/");
		} else {
			if (FileSystem::exist(url)) {
				request->send(SPIFFS, url);
			} else {
				request->redirect("/");
			}
		}
	});

	server->begin();
}

void WifiAP::keepAlive() {
	if (isInit) {
		dns->processNextRequest();
	}
}

#endif